<?php
namespace Deployer;
require 'recipe/common.php';

// Configuration

set('repository', 'https://dealer00@bitbucket.org/dealer00/angular-phonecat.git');
set('shared_files', []);
set('shared_dirs', ['app']);
set('writable_dirs', []);

// Servers

server('production', 'avebmx.nazwa.pl')
    ->user('avebmx')
    ->identityFile()
    ->set('deploy_path', '~/rozklad.avepark.eu');


// Tasks

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo systemctl restart php-fpm.service');
});
// after('deploy:symlink', 'php-fpm:restart');

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

task('test', function() {
	writeln('Hello, world!');
});

task('pwd', function() {
	$result = run('pwd');
	writeln("Current dir: {$result}");
});
